package com.przemekost.recipeapp.domain;

public enum Difficulty {

    EASY, MODERATE, HARD
}
